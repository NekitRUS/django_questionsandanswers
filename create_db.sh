mysql -uroot -e "CREATE DATABASE IF NOT EXISTS qa;"
mysql -uroot -e "CREATE USER 'box'@'%' IDENTIFIED BY '1234';"
mysql -uroot -e "GRANT ALL ON qa.* TO 'box'@'%';"
mysql -u box -p -e "SHOW DATABASES;"
cd web/ask
python manage.py validate
python manage.py syncdb
