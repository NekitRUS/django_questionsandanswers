mv dgango-project/web/ /home/box/
sudo ln -sf /home/box/web/etc/nginx.conf /etc/nginx/sites-enabled/default
sudo ln -sf /home/box/web/etc/gunicorn_hello.conf /etc/gunicorn.d/test-hello
sudo ln -sf /home/box/web/etc/gunicorn.conf /etc/gunicorn.d/test
sudo /etc/init.d/nginx restart
sudo pkill gunicorn
sudo gunicorn --daemon -c /home/box/web/etc/gunicorn_hello.py hello:application
sudo gunicorn --daemon -c /home/box/web/etc/gunicorn.py ask.wsgi
#sudo /etc/init.d/mysql start
sudo service mysql start
