from django.conf.urls import patterns, include, url
from qa.views import test, home, question, popular, new, ask, log_in, signup

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', home, name='home'),
    url(r'^login/$', log_in, name='login'),
    url(r'^signup/$', signup, name='signup'),
    url(r'^question/(?P<id>\d+)/$', question, name='question'),
    url(r'^ask/$', ask, name='ask'),
    url(r'^popular/$', popular, name='popular'),
    url(r'^new/$', new, name='new'),
)
