from django import forms
from qa.models import Question, Answer
from django.contrib.auth.models import User

class AskForm(forms.Form):
    title = forms.CharField(max_length = 255)
    text = forms.CharField(widget = forms.Textarea)

    def save(self):
        question = Question(title = self.cleaned_data['title'], text = self.cleaned_data['text'])
        question.author_id = self._user.id
        question.save()
        return question

class AnswerForm(forms.Form):
    text = forms.CharField(widget = forms.Textarea)
    question = forms.IntegerField(widget = forms.HiddenInput, label = '')
    
    def clean_question(self):
        question = self.cleaned_data['question']
        try:
            question = int(question)
        except:
            raise forms.ValidationError('Question not found')
        return question

    def save(self):
        answer = Answer(text = self.cleaned_data['text'])
        answer.question = Question.objects.get(id = self.cleaned_data['question'])
        answer.author_id = self._user.id
        answer.save()
        return answer

class SignUpForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        widgets = { 'password': forms.PasswordInput, }

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget = forms.PasswordInput)