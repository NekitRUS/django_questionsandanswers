from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User

class QuestionManager(models.Manager):

    def new(self):
        return self.model.objects.order_by('-added_at')

    def popular(self):
        return self.model.objects.order_by('-rating')

class Question(models.Model):
    title = models.CharField(max_length = 255)
    text = models.TextField()
    added_at = models.DateTimeField(auto_now_add = True)
    rating = models.IntegerField(default = 0)
    author = models.ForeignKey(User, null = True, on_delete = models.SET_NULL)
    likes = models.ManyToManyField(User, related_name = 'likes_set')
    objects = QuestionManager()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('question', args = [self.id])

    class Meta:
        db_table = 'questions'
        ordering = ['-added_at']

class Answer(models.Model):
    text = models.TextField()
    added_at = models.DateTimeField(auto_now_add = True)
    author = models.ForeignKey(User, null = True, on_delete = models.SET_NULL)
    question = models.ForeignKey(Question)

    def __unicode__(self):
        return self.text

    class Meta:
        db_table = 'answers'
        ordering = ['-added_at']
