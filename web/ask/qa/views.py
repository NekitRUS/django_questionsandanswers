from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.paginator import Paginator, EmptyPage
from django.contrib.auth import models, authenticate, login
from django.shortcuts import render
from django.core.urlresolvers import reverse
from qa.models import Question, Answer
from qa.forms import AnswerForm, AskForm, SignUpForm, LoginForm

def test(request, *args, **kwargs):
	return HttpResponse('OK')

def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = models.User.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
            user = authenticate(username = request.POST['username'], password = request.POST['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('home'))
    else:
        form = SignUpForm()
    return render(request, 'signup.html', { 'form': form })

def log_in(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        user = authenticate(username = request.POST['username'], password = request.POST['password'])
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('home'))
    else:
        form = LoginForm()
    return render(request, 'login.html', { 'form': form })

def ask(request):
    if request.method == "POST":
        user = request.user
        if user.is_authenticated():
            form = AskForm(request.POST)
            if form.is_valid():
                form._user = user
                question = form.save()
                return HttpResponseRedirect(question.get_absolute_url())
        else:
            return HttpResponseRedirect(reverse('login'))
    else:
        form = AskForm()
    return render(request, 'ask.html', { 'form': form })

def home(request):
    page = paginate(request, Question.objects.new())
    page.paginator.baseurl = reverse('home') + '?page='
    return render(request, 'home.html',
    	{ 'questions': page.object_list,
    	  'page': page, })

def question(request, id):
    if request.method == "POST":
        user = request.user
        if user.is_authenticated():
            form = AnswerForm(request.POST)
            if form.is_valid():
                form._user = user
                answer = form.save()
#                return HttpResponseRedirect(answer.question.get_absolute_url())
        else:
            return HttpResponseRedirect(reverse('login'))     
#    else:
    try:
        question = Question.objects.get(id = id)
    except Question.DoesNotExist:
        raise Http404
    form = AnswerForm(initial = {'question': id})
    try:
        answers = Answer.objects.filter(question = question)
    except Answer.DoesNotExist:
        answers = []
    return render(request, 'question.html', { 'question': question, 'answers': answers, 'form': form, })

def popular(request):
    page = paginate(request, Question.objects.popular())
    page.paginator.baseurl = reverse('popular') + '?page='
    return render(request, 'popular.html',
    	{ 'questions': page.object_list,
    	  'page': page, })

def new(request):
    return home(request)

def paginate(request, qs):
    try:
        limit = int(request.GET.get('limit', 10))
    except ValueError:
        limit = 10
    if limit > 100:
        limit = 10
    try:
        page = int(request.GET.get('page', 1))
    except ValueError:
        raise Http404
    paginator = Paginator(qs, limit)
    try:
        page = paginator.page(page)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    return page