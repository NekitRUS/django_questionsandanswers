def application(environ, start_response):
    status = '200 OK'
    headers = [('Content-Type', 'text/plain')]
    tempList = environ['QUERY_STRING'].split('&')
    start_response(status, headers)
    return '\r\n'.join([i for i in tempList])